# !/bin/sh
# Make script for Wake to Hell
# Licensed under CC0

COMPILER="gcc"

if [ ! $1 ]; then
	echo "Please give an argument for the target frontend. (sdl, saf, etc)"
	exit 1
fi

if [ $1 = "sdl" ]; then
	$COMPILER main_sdl.c -DS3FG_TPE -lSDL2 -lSDL2_mixer -DS3FG_SDL_MIXER $2 -o Fazer
elif [ $1 = "saf" ]; then
	echo "Fazer currently has no SAF version."
fi
