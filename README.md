# Fazer

Fazer is a 3D FPS game made using drummyfish's small3dlib library, inspired by Anarch and Quake. It is programmed similarly to Anarch.

**Currently, the goal is to make an FPS engine that uses small3dlib, uses either meshes (better level fidelity) or 2D maps (better performance and more simplicity) for collision, and feels something like Quake (blast jumping?).**

## Lore

Stranded on the last remaining city in the United Area Among Lenaidoa, the only place to survive the robot invasion of the 22nd century, you and the last remaining men left fighting manage to find a solution to the ongoing havoc, but only one man can remain. One liberator of humankind. You were chosen to execute this plan.

You set foot to the Overbill headquarters in a murderous trance to disable the core reactor and disable the forcefields that kept almost all of mankind cluelessly captive. No bodyarmor. No bullets. This will be daunting.

## What is Fazer anyway?

Fazer is, specifically, a **free as in freedom suckless 3D FPS game made for the benefit of all living beings** that is completely public domain and aims to be easily moddable. It takes what makes Anarch such a beautiful work of software and expands it into a completely 3D space.

Naturally, it requires higher specs than Anarch does, being completely, truly 3D.
