#!/bin/sh

python3 obj2array.py -npistol pistol.obj > ../pistolModel.h
python3 obj2array.py -nsmg smg.obj > ../smgModel.h
python3 img2array.py -npistol PistolTexture.png > ../pistolTexture.h
