#define S3L_PIXEL_FUNCTION drawPixel

#ifndef S3FG_FPS
#define S3FG_FPS 30
#endif

#define S3FG_KEY_UP 1
#define S3FG_KEY_DOWN 1 << 1
#define S3FG_KEY_LEFT 1 << 2
#define S3FG_KEY_RIGHT 1 << 3
#define S3FG_KEY_JUMP 1 << 4
#define S3FG_KEY_FIRE 1 << 5
#define S3FG_KEY_LOOKUP 1 << 6
#define S3FG_KEY_LOOKDOWN 1 << 7

#define S3FG_FLAG_PLAYER_DEAD 1
#define S3FG_FLAG_PLAYER_GROUNDED 1 << 1

#include "small3dlib.h"
#ifdef S3FG_TPE
// experimental TPE integration:
// instead of using the physics from it, we use the SDF-esque functions
// it provides to do our collisions
#include "tinyphysicsengine.h"
#endif

uint8_t gameFrame;

typedef struct {
	S3L_Transform3D bbox; // bbox = player position & collision
	S3L_Vec4 vel, moveDir;
	uint8_t flags;
	uint8_t health, weapon, ammo[4];
} S3FG_Player;

S3FG_Player plr;

S3L_Scene scene;

S3L_Transform3D levelBoxes[32];

uint8_t input;
int16_t mouseLook[2];

uint8_t boxesColliding(S3L_Transform3D *box1, S3L_Transform3D *box2) {
    return (!(
        box2->translation.x >= box1->translation.x + box1->scale.x ||
        box1->translation.x >= box2->translation.x + box2->scale.x ||
        box2->translation.y >= box1->translation.y + box1->scale.y ||
        box1->translation.y >= box2->translation.y + box2->scale.y ||
        box2->translation.z >= box1->translation.z + box1->scale.z ||
        box1->translation.z >= box2->translation.z + box2->scale.z
    ));
};

uint8_t boxCollidesWithLine(S3L_Transform3D *box, S3L_Vec4 point1, S3L_Vec4 point2);

uint8_t boxCollidesWithFace(S3L_Transform3D *box, S3L_Vec4 *face[3]);


void S3FG_playSound(uint8_t sound);

void S3FG_hurtPlayer(uint8_t dmg) {
	plr.health = S3L_clamp(plr.health - dmg, 0, 255);
	// TODO: view roll offset when hurt, plr damage sfx
};

TPE_Unit elevatorHeight;
TPE_Unit ramp[6] = { 1600,0, -500,1400, -700,0 };
TPE_Unit ramp2[6] = { 2000,-5000, 1500,1700, -5000,-500 };

TPE_Vec3 environmentDistance(TPE_Vec3 p, TPE_Unit maxD)
{
	// manually created environment to match the 3D model of it
	TPE_ENV_START( TPE_envAABoxInside(p,TPE_vec3(0,2450,-2100),TPE_vec3(12600,5000,10800)),p )
	TPE_ENV_NEXT( TPE_envAABox(p,TPE_vec3(-5693,0,-6580),TPE_vec3(4307,20000,3420)),p )
	TPE_ENV_NEXT( TPE_envAABox(p,TPE_vec3(-10000,-1000,-10000),TPE_vec3(11085,2500,9295)),p )
	TPE_ENV_NEXT( TPE_envAATriPrism(p,TPE_vec3(-5400,0,0),ramp,3000,2), p)
	TPE_ENV_NEXT( TPE_envAATriPrism(p,TPE_vec3(2076,651,-6780),ramp2,3000,0), p)
	TPE_ENV_NEXT( TPE_envAABox(p,TPE_vec3(7000,0,-8500),TPE_vec3(3405,2400,3183)),p )
	TPE_ENV_NEXT( TPE_envSphere(p,TPE_vec3(2521,-100,-3799),1200),p )
	TPE_ENV_NEXT( TPE_envAABox(p,TPE_vec3(5300,elevatorHeight,-4400),TPE_vec3(1000,elevatorHeight,1000)),p )
	TPE_ENV_NEXT( TPE_envHalfPlane(p,TPE_vec3(5051,0,1802),TPE_vec3(-255,0,-255)),p )
	TPE_ENV_NEXT( TPE_envInfiniteCylinder(p,TPE_vec3(320,0,170),TPE_vec3(0,255,0),530),p )
	TPE_ENV_END
}

void start() {
	gameFrame = 0;
	levelBoxes[0].scale.x = S3L_F * 12;
	levelBoxes[0].scale.y = S3L_F;
	levelBoxes[0].scale.z = S3L_F * 12;
	levelBoxes[0].translation.x = 0;
	levelBoxes[0].translation.y = 0;
	levelBoxes[0].translation.z = 0;
	plr.bbox.translation.y = S3L_F * 4;
};

void step() {
	if (!(plr.flags & S3FG_FLAG_PLAYER_DEAD)) {
		if (input & S3FG_KEY_UP) {
			plr.moveDir.x = -S3L_sin(plr.bbox.rotation.y) / 4;
			plr.moveDir.z = -S3L_cos(plr.bbox.rotation.y - S3L_F / 2) / 4;
			printf("MOVING FORWARD: %i\n", plr.vel.z);
		};

		if (input & S3FG_KEY_DOWN) {
			plr.moveDir.x = S3L_sin(plr.bbox.rotation.y) / 4;
			plr.moveDir.z = S3L_cos(plr.bbox.rotation.y - S3L_F / 2) / 4;
			printf("MOVING BACKWARD: %i\n", plr.vel.z);
		};

		if (input & S3FG_KEY_LEFT) {
			plr.moveDir.z -= S3L_sin(plr.bbox.rotation.y) / 4;
			plr.moveDir.x -= S3L_cos(plr.bbox.rotation.y) / 4;
			printf("STRAFING LEFT: %i\n", plr.vel.z);
		};

		if (input & S3FG_KEY_RIGHT) {
			plr.moveDir.z += S3L_sin(plr.bbox.rotation.y) / 4;
			plr.moveDir.x += S3L_cos(plr.bbox.rotation.y) / 4;
			printf("STRAFING RIGHT: %i\n", plr.vel.z);
		};

		S3L_vec3Normalize(&plr.moveDir);
		plr.moveDir.x /= 8;
		plr.moveDir.z /= 8;
		S3L_vec3Add(&plr.vel, plr.moveDir);
	};

	if (plr.flags & S3FG_FLAG_PLAYER_GROUNDED) {
		plr.vel.x /= 4;
		plr.vel.x += plr.vel.x * 2;
		plr.vel.z /= 4;
		plr.vel.z += plr.vel.z * 2;
	};

	S3L_vec4Set(&plr.moveDir, 0, 0, 0, 0);

	if (input & S3FG_KEY_JUMP) {
		if (plr.flags & S3FG_FLAG_PLAYER_GROUNDED) {
			plr.vel.y = S3L_F / 10;
			plr.bbox.translation.y++;
			S3FG_playSound(0);
			//plr.flags ^= S3FG_FLAG_PLAYER_GROUNDED;
			puts("JUMPED");
		};
	};

	// VERY basic player physics
	S3L_Transform3D plrGroundCollide = plr.bbox;
	plrGroundCollide.translation.y--;
	plr.vel.y -= 3;
	for (uint8_t i = 0; i < 32; i++) {
		if (plrGroundCollide.translation.y < levelBoxes[i].translation.y) {
			plr.vel.y = 0;
			plr.bbox.translation.y = levelBoxes[i].translation.y;
			if (!(plr.flags & S3FG_FLAG_PLAYER_GROUNDED)) plr.flags ^= S3FG_FLAG_PLAYER_GROUNDED;
			//puts("TOUCHING GROUND!");
		} else if (plr.flags & S3FG_FLAG_PLAYER_GROUNDED) plr.flags ^= S3FG_FLAG_PLAYER_GROUNDED;
	};

	TPE_Vec3 plrTPEPos;
	plrTPEPos.x = plr.bbox.translation.x / 2;
	plrTPEPos.y = plr.bbox.translation.y / 2;
	plrTPEPos.z = plr.bbox.translation.z / 2;

	TPE_Vec3 dist = environmentDistance(plrTPEPos, TPE_F);

	if (input & S3FG_KEY_LOOKUP) plr.bbox.rotation.x++;
	if (input & S3FG_KEY_LOOKDOWN) plr.bbox.rotation.x--;

	plr.bbox.rotation.x += mouseLook[0];
	plr.bbox.rotation.y += mouseLook[1];
	if (plr.bbox.rotation.x < -(S3L_F / 4)) plr.bbox.rotation.x = -(S3L_F / 4);
	if (plr.bbox.rotation.x > S3L_F / 4) plr.bbox.rotation.x = S3L_F / 4;

	printf("GROUNDED = %i\n", (plr.flags & S3FG_FLAG_PLAYER_GROUNDED));
	S3L_vec3Add(&plr.bbox.translation, plr.vel);
	scene.camera.transform.translation = plr.bbox.translation;
	scene.camera.transform.rotation = plr.bbox.rotation;
	input = 0;
	mouseLook[0] = 0;
	mouseLook[1] = 0;

	gameFrame++;
	if (gameFrame >= S3FG_FPS) gameFrame = 0;
};
