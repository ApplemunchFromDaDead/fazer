// SDL frontend for an untitled 3D FPS game inspired by Anarch

#include <stdio.h>
#include <stdlib.h>

#include <SDL2/SDL.h>
#ifdef S3FG_SDL_MIXER
#include <SDL2/SDL_mixer.h>
#endif

#define S3L_RESOLUTION_X 320
#define S3L_RESOLUTION_Y 200
#define S3L_PIXEL_FUNCTION drawPixel

#define S3L_FLAT 0
#define S3L_NEAR_CROSS_STRATEGY 3
#define S3L_PERSPECTIVE_CORRECTION 2
#define S3L_SORT 0
#define S3L_STENCIL_BUFFER 0
#define S3L_Z_BUFFER 1

#define S3L_MAX_TRIANGLES_DRAWn 2048

#include "game.h"

#include "levelModel.h" // FOR TESTIN PURPOSES!!!!!!!!!
#include "pistolModel.h"
#include "smgModel.h"
#include "pistolTexture.h"

SDL_Window *win;
SDL_Renderer *render;

SDL_Texture *texture;
uint32_t pixels[S3L_RESOLUTION_X * S3L_RESOLUTION_Y];

S3L_Scene scene;
S3L_Model3D models[128];

#ifdef S3FG_SDL_MIXER
Mix_Chunk *sfx[7];
#endif

static inline void setPixel(int x, int y, uint8_t red, uint8_t green, uint8_t blue)
{
  uint8_t *p = ((uint8_t *) pixels) + (y * S3L_RESOLUTION_X + x) * 4 + 1;

  *p = blue;
  ++p;
  *p = green;
  ++p;
  *p = red;
}

uint32_t prevTriangle = -1;
S3L_Vec4 uv0, uv1, uv2;

void sampleTexture(const uint8_t *tex, int32_t u, int32_t v, uint8_t *r, uint8_t *g, uint8_t *b)
{
  u = S3L_wrap(u,64);
  v = S3L_wrap(v,64);

  const uint8_t *t = tex + (v * 64 + u) * 3;
  *r = *t;
  t++;
  *g = *t;
  t++;
  *b = *t;
}

const S3L_Unit *uvs;
const S3L_Index *uvIndices;

void drawPixel(S3L_PixelInfo *p)
{
  uint8_t
  r = 128,
  g = 128,
  b = 128;

  if (p->triangleID != prevTriangle) {
	uvs = pistolUVs;
	uvIndices = pistolUVIndices;
	S3L_getIndexedTriangleValues(p->triangleIndex, uvIndices, uvs, 2, &uv0, &uv1, &uv2);
	prevTriangle = p->triangleID;
  };

  if (p->modelIndex == 1) {
	S3L_Unit uv[2];

	uv[0] = S3L_interpolateBarycentric(uv0.x,uv1.x,uv2.x,p->barycentric);
	uv[1] = S3L_interpolateBarycentric(uv0.y,uv1.y,uv2.y,p->barycentric);

	sampleTexture(pistolTexture,uv[0] / 8,uv[1] / 8,&r,&g,&b);
  };
  int16_t f = ((p->depth - S3L_NEAR) * 255) / (S3L_F * 64);

  r = S3L_clamp(((int16_t) r) - f,0,255);
  g = S3L_clamp(((int16_t) g) - f,0,255);
  b = S3L_clamp(((int16_t) b) - f,0,255);

  setPixel(p->x,p->y,r,g,b); 
};

// define Fazer functions

void S3FG_playSound(uint8_t sound) {
	#ifdef S3FG_SDL_MIXER
	Mix_PlayChannel(-1, sfx[sound], 0);
	#endif
};

void draw() {
	S3L_newFrame();
	S3L_drawScene(scene);
	SDL_UpdateTexture(texture, NULL, &pixels, S3L_RESOLUTION_X * sizeof(uint32_t));
	SDL_RenderClear(render);
	SDL_RenderCopy(render, texture, NULL, NULL);
	SDL_RenderPresent(render);
	for (uint16_t i = 0; i < S3L_RESOLUTION_X * S3L_RESOLUTION_Y; i++) pixels[i] = 200;
};

void main() {
	#ifdef S3FG_SDL_MIXER
	#define S3FG_SDLFLAGS SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_EVENTS | MIX_INIT_MID
	#else
	#define S3FG_SDLFLAGS SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_EVENTS
	#endif

	if (SDL_Init(S3FG_SDLFLAGS != 0))
			return;
	#undef S3FG_SDLFLAGS

	win = SDL_CreateWindow("Fazer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, S3L_RESOLUTION_X << 1, S3L_RESOLUTION_Y << 1, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	render = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	texture = SDL_CreateTexture(render, SDL_PIXELFORMAT_RGBX8888, SDL_TEXTUREACCESS_STATIC, S3L_RESOLUTION_X, S3L_RESOLUTION_Y);

	#ifdef S3FG_SDL_MIXER
	if (Mix_OpenAudio(44410, MIX_DEFAULT_FORMAT, 1, 2048) != 0)
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_WARNING, "No audio!", "Fazer couldn't find a usable audio system. Check your audio devices!", NULL);

	Mix_Music *music[2];

	sfx[0] = Mix_LoadWAV("sfx/plrjump.wav");
	sfx[1] = Mix_LoadWAV("sfx/plrhurt1.wav");
	sfx[2] = Mix_LoadWAV("sfx/plrhurt2.wav");
	sfx[3] = Mix_LoadWAV("sfx/plrhurt3.wav");
	sfx[4] = Mix_LoadWAV("sfx/plrhurt4.wav");
	sfx[5] = Mix_LoadWAV("sfx/plrdie1.wav");
	sfx[6] = Mix_LoadWAV("sfx/plrdie2.wav");

	music[0] = Mix_LoadMUS("music/map0.mid");
	music[1] = Mix_LoadMUS("music/map1.mid");

	Mix_PlayMusic(music[0], -1);
	#endif

	SDL_RenderSetLogicalSize(render, 320, 200);

	// load Mixer-exclusive stuff here
	
	uint8_t *keystates;
	keystates = SDL_GetKeyboardState(NULL);

	SDL_SetRelativeMouseMode(SDL_TRUE); // capture mouse
	
	S3L_sceneInit(&models, 128, &scene);
	levelModelInit();
	pistolModelInit();
	smgModelInit();
	models[0] = levelModel;
	models[0].transform.scale.x = S3L_F * 2;
	models[0].transform.scale.y = S3L_F * 2;
	models[0].transform.scale.z = S3L_F * 2;
	models[1] = pistolModel;

	uint8_t running = 1;
	SDL_Event event;
	start();

	models[2].transform = levelBoxes[0];
	while (running) {
		while (SDL_PollEvent(&event)) {
			switch (event.type)
			{
				case SDL_QUIT: running = 0; break;
				case SDL_MOUSEMOTION:
					       mouseLook[0] = -event.motion.yrel;
					       mouseLook[1] = -event.motion.xrel;
					       break;
			}
		}

		if (keystates[SDL_SCANCODE_W]) input ^= S3FG_KEY_UP;
		if (keystates[SDL_SCANCODE_S]) input ^= S3FG_KEY_DOWN;
		if (keystates[SDL_SCANCODE_A]) input ^= S3FG_KEY_LEFT;
		if (keystates[SDL_SCANCODE_D]) input ^= S3FG_KEY_RIGHT;
		if (keystates[SDL_SCANCODE_UP]) input ^= S3FG_KEY_LOOKUP;
		if (keystates[SDL_SCANCODE_DOWN]) input ^= S3FG_KEY_LOOKDOWN;
		if (keystates[SDL_SCANCODE_SPACE]) input ^= S3FG_KEY_JUMP;

		if (keystates[SDL_SCANCODE_ESCAPE]) running = 0;
		step();
		models[1].transform.translation = plr.bbox.translation;
		models[1].transform.rotation = plr.bbox.rotation;
		draw();
		SDL_Delay((float)1 / S3FG_FPS);
	};

};
